package tdt4250.webpageBuilder;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import course.Course;
import course.CourseInfo;
import course.CourseInstance;
import course.CoursePackage;
import course.CreditReduction;
import course.EvaluationForm;
import course.Person;
import course.Study;
import course.Timetable;
import course.util.CourseResourceFactoryImpl;


public class WebpageBuilder {
	
	public static void main(String[] args) throws IOException{
		Course c = (args.length > 0 ? getCourse(args[0]) : getSampleCourse());
		String html = generateHtml(c);
		System.out.println(html);
		try {
			if(args.length > 1) {
				writeFile(args[1]+"/"+c.getCode()+".html", html);
			}else {
				writeFile("Output/"+c.getCode()+".html", html);
			}
		}catch(Exception e) {
			System.out.println("Can't find the Output folder. Please make one inside this folder.");
		}
	}
	
	public static Course getCourse(String uri) {
		System.out.println(uri);
		if(uri.indexOf("file:/") == -1) {
			uri = "file:/"+uri;
		}
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("course", new CourseResourceFactoryImpl());
		ResourceSet resSet = new ResourceSetImpl();
		resSet.getPackageRegistry().put(CoursePackage.eNS_URI, CoursePackage.eINSTANCE);
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("course", new CourseResourceFactoryImpl());
		Resource resource = resSet.getResource(URI.createURI(uri), true);
		for (EObject eObject : resource.getContents()) {
			if (eObject instanceof Course) {
				return (Course) eObject;
			}
		}
		return null;

	}
	
	public static Course getSampleCourse() throws IOException {
		return getCourse(WebpageBuilder.class.getClassLoader().getResource("SampleCourse.course").toString());
	}
	
	private static void writeFile(String filename, String data) throws FileNotFoundException, UnsupportedEncodingException {
		PrintWriter w = new PrintWriter(filename, "UTF-8");
		w.print(data);
		w.close();
	}
	
	private static String generateHtml(Course c) {
		//System.out.println(c);
		String html = "<!DOCTYPE html>\r\n" + 
				"<html>\r\n";
		html += addHead(c);
		html += addBody(c);
		html += "</html>";
		
		return html;
	}
	
	private static String addHead(Course c) {
		return "<head>\r\n" + 
				"    <title>"+c.getCode()+" - "+c.getName()+"</title>\r\n"+
				"    <style>\r\n" + 
				"        *{\r\n" + 
				"            margin: 0;\r\n" + 
				"            padding: 0;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        html, body{\r\n" + 
				"            width: 100%;\r\n" + 
				"            height: 100%;\r\n" + 
				"            font-family: sans-serif;\r\n" + 
				"            color: #202020;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        /***\r\n" + 
				"         basic\r\n" + 
				"         */\r\n" + 
				"\r\n" + 
				"ul{\r\n"+
				"	list-style: none;\r\n"+
				"}\r\n"+
				"\r\n"+
				"        table {\r\n" + 
				"            font-family: arial, sans-serif;\r\n" + 
				"            border-collapse: collapse;\r\n" + 
				"            width: 100%;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        td, th {\r\n" + 
				"            border: 1px solid #dddddd;\r\n" + 
				"            text-align: left;\r\n" + 
				"            padding: 8px;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        tr:nth-child(even) {\r\n" + 
				"            background-color: #dddddd;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        /***\r\n" + 
				"         main\r\n" + 
				"         */\r\n" + 
				"\r\n" + 
				"        #header{\r\n" + 
				"            padding: 20px 10px;\r\n" + 
				"            display: flex;\r\n" + 
				"            justify-content: space-between;\r\n" + 
				"            align-items: center;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        #title{\r\n" + 
				"            font-size: 40px;\r\n" + 
				"            color: #1fa0ff;\r\n" + 
				"            text-shadow: 0 1px rgba(0, 0, 0, 0.8);\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        #content{\r\n" + 
				"            display: flex;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        .contentTitle{\r\n" + 
				"            font-size: 30px;\r\n" + 
				"            font-weight: 600;\r\n" + 
				"            color: #616161;\r\n" + 
				"            margin-bottom: 10px;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        #col1{\r\n" + 
				"            padding-left: 30px;\r\n" + 
				"            width: calc(100% - 300px);\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        #col2{\r\n" + 
				"            width: 300px;\r\n" + 
				"            padding: 10px;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        .InfoBlock > div{\r\n" + 
				"            padding-bottom: 20px;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        .InfoBlock > div > span:nth-child(1){\r\n" + 
				"            display: block;\r\n" + 
				"            font-size: 25px;\r\n" + 
				"            color: #585858;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        /***\r\n" + 
				"           infobox\r\n" + 
				"         */\r\n" + 
				"        .spacer{\r\n" + 
				"            width: 100%;\r\n" + 
				"            height: 15px;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        .InfoBlock{\r\n" + 
				"            margin-bottom: 50px;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        .InfoBox{\r\n" + 
				"            border: 1px solid gray;\r\n" + 
				"            border-radius: 10px;\r\n" + 
				"            margin-bottom: 10px;\r\n" + 
				"            background-color: white;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        .InfoBox >div:nth-child(1){\r\n" + 
				"            background-color: gray;\r\n" + 
				"            /* border-radius: 10px; */\r\n" + 
				"            border-top-left-radius: 10px;\r\n" + 
				"            border-top-right-radius: 10px;\r\n" + 
				"            padding: 5px 10px;\r\n" + 
				"            color: #f7f7f7;\r\n" + 
				"            font-weight: 600;\r\n" + 
				"        }\r\n" + 
				"\r\n" + 
				"        .InfoBox > div:nth-child(2){\r\n" + 
				"            padding: 10px 5px;\r\n" + 
				"        }\r\n" + 
				"    </style>\r\n" + 
				"</head>\r\n";
	}
	
	private static String addBody(Course c) {
		String body = "<body>\r\n";
		body += makeHeader(c.getCode()+" - "+c.getName(), c.getCourseInstances());
		body += "<div id=\"content\">\r\n" + 
				"        <div id=\"col1\">\r\n";
		body += makeAboutCourse(c);
		body += makeTimeTable(c.getCourseInstances().get(0).getTimeTables());
		body += "</div>\r\n"
				+ "<div id=\"col2\">\r\n";
		body += makeInfoBoxes(c);
		body += "        </div>\r\n" + 
				"    </div>\r\n"
				+ "</body>\r\n";
		return body;
	}
	
	private static String makeHeader(String title, List<CourseInstance> instances) {
		String instancesList = "";
		for(int i=0; i<instances.size(); i++) {
			instancesList += "<option>"+instances.get(i).getSemester()+" "+instances.get(i).getYear()+"</option>\r\n";
		}
		
		return "<div id=\"header\">\r\n" + 
				"        <div id=\"title\">\r\n" + 
				"            <span>"+title+"</span>\r\n" + 
				"        </div>\r\n" + 
				"        <div>\r\n" + 
				"            <select>\r\n" + 
				instancesList +
				"            </select>\r\n" + 
				"        </div>\r\n" + 
				"    </div>\r\n";
	}
	
	private static String makeAboutCourse(Course c) {
		
		String evaluations = "";
		String creditReductions = "";
		CourseInstance curInstance = c.getCourseInstances().get(0);
		CourseInfo courseInfo = c.getCourseInfo();
		
		for(int i=0; i<curInstance.getEvaluationForms().size(); i++) {
			EvaluationForm curE = curInstance.getEvaluationForms().get(i);
			evaluations += "<tr>\r\n" + 
					"                            <td>"+curE.getType()+"</td>\r\n" + 
					"                            <td>"+(int)(curE.getWeight()*100)+"/100</td>\r\n" + 
					"                            <td>"+(curE.getDuration() != 0 ? curE.getDuration():"")+"</td>\r\n" + 
					"                            <td>"+(curE.getExaminationAid() != null ? "<a href=\""+curE.getExaminationAid().getWebsite()+"\" title=\""+curE.getExaminationAid().getInfoText()+"\">"+curE.getExaminationAid().getChar()+"</a>":"")+"</td>\r\n" + 
					"                        </tr>\r\n";
		}
		
		for(int i=0; i<c.getCreditReductions().size(); i++) {
			CreditReduction curC = c.getCreditReductions().get(i);
			creditReductions += "<tr>\r\n" + 
					"                            <td>"+curC.getCourseCode()+"</td>\r\n" + 
					"                            <td>"+curC.getReduction()+"</td>\r\n" + 
					"                            <td>"+(curC.getFrom() != null ? new SimpleDateFormat("MM.dd.yyyy").format(curC.getFrom()) : "")+"</td>\r\n" + 
					"                            <td>"+(curC.getTo() != null ? new SimpleDateFormat("MM.dd.yyyy").format(curC.getTo()) : "")+"</td>\r\n" + 
					"                        </tr>\r\n";
		}
		
		return "<div class=\"contentTitle\">\r\n" + 
				"                <span>Om emnet</span>\r\n" + 
				"            </div>\r\n" + 
				"            <div class=\"InfoBlock\">\r\n" + 
				"                <div>\r\n" + 
				"                    <span>Vurderingsordning</span>\r\n" + 
				"                    <table>\r\n" + 
				"                        <tr>\r\n" + 
				"                            <th>Vurderingsform</th>\r\n" + 
				"                            <th>Vekting</th>\r\n" + 
				"                            <th>Varight</th>\r\n" + 
				"                            <th>Hjelpemidler</th>\r\n" + 
				"                        </tr>\r\n" + 
				evaluations+
				"                    </table>\r\n" + 
				"                </div>\r\n" + 
				"                <div>\r\n" + 
				"                    <span>Faglig innhold</span>\r\n" + 
				"                    <span>"+courseInfo.getContent()+"</span>\r\n" + 
				"                </div>\r\n" + 
				"                <div>\r\n" + 
				"                    <span>Læringsutbytte</span>\r\n" + 
				"                    <span>"+courseInfo.getLearningOutcome()+"</span>\r\n" + 
				"                </div>\r\n" + 
				"                <div>\r\n" + 
				"                    <span>Læringsformer og aktiviteter</span>\r\n" + 
				"                    <span>"+courseInfo.getLearningMethodsAndActivities()+"</span>\r\n" + 
				"                </div>\r\n" + 
				"                <div>\r\n" + 
				"                    <span>Obligatoriske aktiviteter</span>\r\n" + 
				"                    <span>"+courseInfo.getCompulsoryAssignments()+"</span>\r\n" + 
				"                </div>\r\n" + 
				"                <div>\r\n" + 
				"                    <span>Mer om vurdering</span>\r\n" + 
				"                    <span>"+courseInfo.getFurtherOnEvaluation()+"</span>\r\n" + 
				"                </div>\r\n" + 
				"                <div>\r\n" + 
				"                    <span>Spesielle vilkår</span>\r\n" + 
				"                    <span>"+courseInfo.getSpecificConditions()+"</span>\r\n" + 
				"                </div>\r\n" + 
				"                <div>\r\n" + 
				"                    <span>Anbefalte forkunnskaper</span>\r\n" + 
				"                    <span>"+courseInfo.getRecommendedPreviousKnowledge()+"</span>\r\n" + 
				"                </div>\r\n" + 
				"                <div>\r\n" + 
				"                    <span>Kursmateriell</span>\r\n" + 
				"                    <span>"+courseInfo.getCourseMaterials()+"</span>\r\n" + 
				"                </div>\r\n" + 
				"                <div>\r\n" + 
				"                    <span>Studiepoengreduksjon</span>\r\n" + 
				"                    <table>\r\n" + 
				"                        <tr>\r\n" + 
				"                            <th>Emnekode</th>\r\n" + 
				"                            <th>Reduksjon</th>\r\n" + 
				"                            <th>Fra</th>\r\n" + 
				"                            <th>Til</th>\r\n" + 
				"                        </tr>\r\n" + 
				creditReductions +                  
				"                    </table>\r\n" + 
				"                </div>\r\n" + 
				"            </div>";
		
	}
	
	private static String makeTimeTable(List<Timetable> tables) {
		String timeTables = "";
		
		for(int i=0; i<tables.size(); i++) {
			Timetable t = tables.get(i);
			String plannedFor = "";
			for(int j=0; j<t.getPlannedFor().size(); j++) {
				Study s = t.getPlannedFor().get(j);
				plannedFor += "<a title=\""+s.getName()+"\" href=\""+s.getWebsite()+"\">"+s.getShortName()+"</a>\r\n";
			}
			
			
			timeTables += 	"                    <tr>\r\n" + 
					"                        <td>"+t.getDay()+"</td>\r\n" + 
					"                        <td>"+t.getTimeFrom()+" - "+t.getTimeTo()+"</td>\r\n" + 
					"                        <td>"+t.getWeekFrom()+"-"+t.getWeekTo()+"</td>\r\n" + 
					"                        <td>"+t.getActivity()+"</td>\r\n" + 
					"                        <td>"+plannedFor+"</td>\r\n" + 
					"                        <td>"+"<span style=\"display:block\"> <a href=\""+t.getRoom().getMazemapUrl()+"\">"+t.getRoom().getName()+"</a> </span>"+"</td>\r\n" + 
					"                    </tr>\r\n";
		}
		
		return "<div class=\"contentTitle\">\r\n" + 
				"                <span>Timeplan</span>\r\n" + 
				"            </div>\r\n" + 
				"            <div class=\"InfoBlock\">\r\n" + 
				"                <table>\r\n" + 
				"                    <tr>\r\n" + 
				"                        <th>Ukedag</th>\r\n" + 
				"                        <th>Tid</th>\r\n" + 
				"                        <th>Uker</th>\r\n" + 
				"                        <th>Type</th>\r\n" + 
				"                        <th>Planlagt for</th>\r\n" + 
				"                        <th>Rom</th>\r\n" + 
				"                    </tr>\r\n" + 
				timeTables +
				"                </table>\r\n" + 
				"            </div>";
	}

	private static String makeInfoBoxes(Course c) {
		
		CourseInstance ci = c.getCourseInstances().get(0);
		String lecturers = "";
		String coordinator = "";
		
		for(int i=0; i<ci.getStaff().getLecturers().size(); i++) {
			Person p = ci.getStaff().getLecturers().get(i);
			lecturers += "<li><a href=\""+p.getWebsite()+"\">"+p.getFirstName()+" "+p.getLastName()+"</a></li>";
		}
		
		if(ci.getStaff().getCoordinator() != null) {
			Person p = ci.getStaff().getCoordinator();
			coordinator = "<li><a href=\""+p.getWebsite()+"\">"+p.getFirstName()+" "+p.getLastName()+"</a></li>";
		}
		
		
		return "<div class=\"InfoBox\">\r\n" + 
				"                <div><span>Fakta om emnet</span></div>\r\n" + 
				"                <div>\r\n" + 
				"                    <div><span>Studiepoeng:</span><span>"+c.getCredits()+"</span></div>\r\n" + 
				"                    <div><span>Studienivå:</span><span>"+c.getLevel()+"</span></div>\r\n" + 
				"                </div>\r\n" + 
				"            </div>\r\n" + 
				"            <div class=\"InfoBox\">\r\n" + 
				"                <div><span>Undervisning</span></div>\r\n" + 
				"                <div>\r\n" + 
				"                    <div><span>Undervises:</span><span>"+ci.getSemester()+" "+ci.getYear()+"</span></div>\r\n" + 
				"                    <div class=\"spacer\"></div>\r\n" + 
				"                    <div><span>Forelesningstimer:</span><span>"+ci.getCourseWork().getLectureHours()+"</span></div>\r\n" + 
				"                    <div><span>Øvingstimer:</span><span>"+ci.getCourseWork().getLabHours()+"</span></div>\r\n" + 
				"                    <div><span>Fordypningstimer:</span><span>"+ci.getCourseWork().getSpecializationHours()+"</span></div>\r\n" + 
				"                    <div class=\"spacer\"></div>\r\n" + 
				"                    <div><span>Undervisningsspråk:</span><span>"+ci.getLanguage()+"</span></div>\r\n" + 
				"                    <div class=\"spacer\"></div>\r\n" + 
				"                    <div><span>Sted:</span><span>"+ci.getLocation()+"</span></div>\r\n" + 
				"                </div>\r\n" + 
				"            </div>\r\n" + 
				"            <div class=\"InfoBox\">\r\n" + 
				"                <div><span>Kontaktinformasjon</span></div>\r\n" + 
				"                <div>\r\n" + 
				"                    <div>\r\n" + 
				"                        <span>Emneansvarlig/koordinator:</span>\r\n" + 
				"                        <ul id=\"coordinatorList\">"+coordinator+"</ul>\r\n" + 
				"                    </div>\r\n" + 
				"                    <div class=\"spacer\"></div>\r\n" + 
				"                    <div>\r\n" + 
				"                        <span>Faglærer(e):</span>\r\n" + 
				"                        <ul id=\"lecturerList\">"+lecturers+"</ul>\r\n" + 
				"                    </div>\r\n" + 
				"                    <div class=\"spacer\"></div>\r\n" + 
				"                    <div>\r\n" + 
				"                        <span>Ansvarlig enhet</span>\r\n" + 
				"                        <br>\r\n" + 
				"                        <a href=\""+c.getDepartment().getWebsite()+"\">"+c.getDepartment().getName()+"</a>\r\n" + 
				"                    </div>\r\n" + 
				"                    <div class=\"spacer\"></div>\r\n" + 
				"                    <div><span>Telefon:</span><span>"+c.getDepartment().getPhone()+"</span></div>\r\n" + 
				"                </div>\r\n" + 
				"            </div>\r\n";
	}
}
